import React from 'react';
import {
  StatusBar,
} from 'react-native';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import {
  Provider
} from 'react-redux'

import {
  PersistGate
} from 'redux-persist/integration/react';
import {
  store,
  persistor
} from "./src/Redux/store";
import WeatherSummary from './src/Screens/WeatherSummary';
import EmployeeList from './src/Screens/EmployeeList';
import MainLoader from './src/Components/MainLoader';
import Home from './src/Screens/Home';
const Root = createStackNavigator();




const RootStack = () => {
  return (
    <Root.Navigator screenOptions={{ header: () => false }} >
      <Root.Screen name="Home"  component={Home} />
      <Root.Screen name="WeatherScreen" component={WeatherSummary} />
      <Root.Screen name="EmployeeScreen" component={EmployeeList} />


    </Root.Navigator>
  )
}

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: "#FFF"
  }
}
const App = () => {

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <NavigationContainer theme={MyTheme}>
          <StatusBar translucent={true} backgroundColor="transparent" />

          <MainLoader />
          <RootStack />


        </NavigationContainer>
      </PersistGate>
    </Provider>
  )
}

export default App;
