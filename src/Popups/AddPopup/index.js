import React, { Component } from 'react';
import {
  View,
  Modal,
  Text,
} from 'react-native';
import styles from './styles';
import Button from '../../Components/Button';
import TouchableHOC from '../../Components/TouchableHOC';
import vw from '../../Units/vw'
import vh from '../../Units/vh'
import MainInput from '../../Components/MainInput'
import { BlurView, } from "@react-native-community/blur";
import Utils from '../../Utils';


export default class AddPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: this.props.value ? true : false,
      text: '',
      visible: false,
      imagesShown: false,
      name: '',
      age: '',
      id: ''
    };
  }

  show = (data) => {
    console.log("on open pouup: ", data);
    if (data) {
      this.setState((p) => {
        return {
          ...p,
          visible: true,
          name: data.name,
          age: data.age,
          id: data.id
        };
      });
    } else
      this.setState((p) => {
        return {
          ...p,
          visible: true,
        };
      });
  };
  hide = () => {
    // console.log("hide");

    this.setState((p) => {
      return {
        ...p,
        visible: false,
      };
    });
    if (this.props.onCross) {
      this.props.onCross()
    }
  };


  _onActionSuccess = (data) => {
    this.props.onSuccess(data, success => {
      alert(success)
      this.setState({
        name: '',
        age: ''
      })
      this.hide()
    }, fail => {
      alert("Employee was not added successfully")
      this.hide()

    })
  }

  onSuccess = () => {

    if (this.props.onSuccess) {

      if (this.props.update) {
        let data = {
          name: this.state.name,
          id: this.state.id,
          age: this.state.age
        }
        console.log("on update pouup: ", data);
        this._onActionSuccess(data)
        return
      } else {

        if (!this.state.name) {
          return alert("Please enter a name")

        }
        if (!Utils.regex.alphabets.test(this.state.name)) {
          return alert("Please enter a valid name")

        }

        if (!this.state.age) {
          return alert("Age enter an age")
        }
        if (!Utils.regex.numbers.test(this.state.age)) {
          return alert("Please enter a valid age")

        }



        let data = {
          name: this.state.name,
          age: this.state.age
        }

        this._onActionSuccess(data)

      }
    }

  }

  onStateChange = (type, change) => {
    this.setState({

      [type]: change

    })
  }

  render() {
    return (
      <Modal
        key={'cbt'}
        visible={this.state.visible}
        transparent={true}
        animationType="fade">
        <BlurView style={styles.blur} blurType="dark" blurAmount={10} />
        <View style={styles.cardCont}>
          <View style={styles.card}>


            <Text onPress={this.hide} style={styles.title}> {this.props.update ? "Update Employee" : "Add New Employee"}</Text>

            <View style={{ paddingHorizontal: vw * 3, width: "100%" }}>

              <MainInput style={{ width: 67 * vw, marginBottom: vh * 2 }}
                fieldStyle={{ width: 55 * vw, }}
                // secureTextEntry={true}
                onChangeText={(name) => this.onStateChange('name', name)}
                value={this.state.name}
                label="Name"
                placeholder={"Enter name"}
              />
              <MainInput style={{ width: 67 * vw, marginBottom: vh * 2 }}
                fieldStyle={{ width: 55 * vw, }}
                label="Age"
                value={this.state.age}
                onChangeText={(age) => this.onStateChange('age', age)}
                placeholder={"Enter Age"}
              />
            </View>
            <Button title={this.props.title ? this.props.title : 'Create User'} onPress={this.onSuccess} styles={styles.btn} />

            <Text onPress={this.hide} style={styles.title}> Go Back</Text>

          </View>

        </View>
      </Modal>
    );
  }
}
