import { StyleSheet } from 'react-native'
import vw from '../../Units/vw'
import vh from '../../Units/vh'
export default StyleSheet.create({
   cardCont: { flex: 1, justifyContent: "center", alignItems: "center" },
   card: {
      backgroundColor: "white", borderRadius: vw * 2, paddingBottom: vw * 3, width: vw * 80, paddingTop: vh * 3, paddingHorizontal: vw * 3.5,
      alignItems: "center"
   },
   cross: { width: vw * 3, height: vw * 3 },
   detailRow: { flexDirection: "row", marginBottom: vh * 2.5, alignItems: "center", justifyContent: "space-between" },
   detailCol: { width: "70%" },
   detailCol1: { width: "30%" },
   detailLabel: { color: "#424953", fontSize: vh * 1.5 },
   dertailVal: { color: "#ED1C24", fontSize: vh * 1.5 },
   btn: { width: "55%", marginBottom: vh * 2, marginTop: vh * 2 },
   title: { color: "#ED1C24", fontSize: vh * 2.2, marginBottom: vh * 3, textAlign: "center" },
   blur: { position: "absolute", width: vw * 100, height: vh * 100 }

})