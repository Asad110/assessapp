import Utils from '../Utils';
import axios from 'axios';

const base_url = Utils.base_url;


const getMessage = (data, callback) => {

    if (data.hasOwnProperty('errors')) {
        var error = data.errors[Object.keys(data.errors)[0]]
        callback(error[0])
    }
    else if (data.message) {
        if (Array.isArray(data.message) === true) {

            callback(data.message[0])

        } else {

            callback(data.message)

        }
    } else if (data.error) {

        callback(data.error)

    } else {
        callback('Something went wrong, try again later')
    }

}

export default {
    get: (endpoint = '', success, error, params) => {


        let parameter = ""
        let index = 0
        if (params) {
            Object.entries(params).forEach(
                ([key, value]) => {
                    let val = index === 0 ? "?" + key + "=" + value : "&" + key + "=" + value
                    let prev = parameter
                    parameter = prev + val
                    index++
                }
            )
        }

        let url = base_url + endpoint

        axios.get(url)
            .then(data => {
                if (!data) {
                    getMessage("Something went wrong.")
                    return
                }
                if (data?.status) {
                    switch (data.status) {
                        case 200: {
                            success(data)
                            break;
                        }
                        case 201: {
                            success(data)
                            break;
                        }
                        case 401: {
                            getMessage(data, error)
                            break;
                        }
                        default: {
                            getMessage(data, error)
                            break
                        }

                    }
                } else {
                    success(data)
                }

            }).catch(e => {
                getMessage(e, error)
            })
    },

}
