import { StyleSheet } from "react-native";
import vh from "../../Units/vh";
import vw from "../../Units/vw";
export default style = StyleSheet.create({
    container: {
        width: 45 * vw,
        backgroundColor: "#FFF",
        marginRight: 3 * vw,
        paddingVertical: 2 * vh,
        borderRadius: vw * 2.5,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 1 * vw,
            height: 1 * vw,
        },
        shadowOpacity: 0.17,
        shadowRadius: 1.65 * vw,

        elevation: 6,
    },
    username: {
        fontSize: vh * 1.7,
        color: "#424953",
        fontWeight: 'bold'
    },
    priceRow: {
        paddingTop: 1 * vh,
        flexDirection: "row",
        bottom: 0.6 * vh
    },
    priceLabel: {
        fontSize: vh * 1.7,
        color: "#999999"
    },
    price: {
        fontSize: vh * 1.7,
        color: "#ED1C24"
    },
    userImg: {
        width: 7.5 * vh,
        height: 7.5 * vh,
        borderRadius: 7.5 / 2 * vh,
        resizeMode: "cover",
        marginBottom: 0.5 * vh
    }
})