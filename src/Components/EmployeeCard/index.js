import React from 'react'
import {
    Text,
    View,
    Image
} from 'react-native'
import styles from './styles'
import TouchableHOC from '../TouchableHOC'
import Button from '../Button'
import vw from '../../Units/vw'
import vh from '../../Units/vh'


export default EmployeeCard = ({ item, index, onDeletePressed, onUpdatePressed }, props) => {
    console.log("each employee: ", item);
    return (
        <View style={[styles.container, props.styles, { borderWidth: item.active ? 2 : 0, borderColor: "#ED1C24" }]} onPress={props.onPress} >
            {/* <Image source={{ uri: props.item.image }} style={styles.userImg} /> */}

            <Text style={[styles.username, props.labelStyle]} numberOfLines={1}>{item.name}</Text>
            <View style={styles.priceRow}>
                <Text style={[styles.priceLabel, props.labelStyle]}>Age: </Text>
                <Text style={[styles.price, props.labelStyle]}>{item.age}</Text>

            </View>
            <View style={{ flexDirection: 'row', width: 30 * vw, justifyContent: 'space-between', marginTop: 1 * vh }}>
                <Button
                    title="Delete"
                    styles={{ height: 4 * vh, width: 14 * vw, backgroundColor: "red" }}
                    onPress={() => onDeletePressed(index)}
                    labelStyle={{ fontSize: 1.7 * vh }}
                />
                <Button
                    title="Update"
                    styles={{ height: 4 * vh, width: 14 * vw, backgroundColor: "green", }}
                    onPress={() => onUpdatePressed(index)}
                    labelStyle={{ fontSize: 1.7 * vh }}


                />
            </View>
        </View>
    )
}