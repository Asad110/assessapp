import React from 'react'
import {
    Text,
    View,
    TextInput
} from 'react-native'
import styles from './styles'
import Ripple from 'react-native-material-ripple'
import vh from '../../Units/vh'
import vw from '../../Units/vw'


class MainInput extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            show: false
        }
    }
    toggle = () => {
        this.setState({ show: !this.state.show })
    }
    
    renderLabel = () => {
        if (this.props.label) {

            return (
                <View style={[styles.labelContainer, this.props.labelStyle]}>
                    <Text style={[styles.label,this.props.labelTextStyle]}>{this.props.label}</Text>
                    <Text style={[styles.label, styles.redLabel]}>{this.props.errorText}</Text>
                </View>
            )
        }
    }
    focus = () => {
        this.input.focus()
    }
    render() {
        var secure = false
        if (this.props.secureTextEntry === true && this.state.show === false) {
            secure = true
        } else {
            secure = false
        }
        return (
            <View>
                {this.renderLabel()}
                <View style={[styles.container,  { borderWidth: this.props.error ? 1.5 : 0, borderColor: "#ED1C24" },this.props.style,]}>
                    {this.props.leftIcon && <IconButton iconStyle={styles.leftIcon} icon={this.props.leftIcon} />}
                    <TextInput
                    
                        placeholderTextColor="#999999"
                        {...this.props}
                        secureTextEntry={secure}
            selectionColor="#ED1C24"
            textAlignVertical='top'
                        style={[styles.field,

                        {
                            
                            
                            width: this.props.leftIcon && this.props.secureTextEntry ? "77%" : this.props.leftIcon || this.props.rightIcon || this.props.secureTextEntry ? "86%" : "100%",
                            marginLeft: this.props.leftIcon ? 0 : 3 * vw,
                        }, this.props.fieldStyle,
                    {textAlignVertical:'top',}
                    ]}
                        ref={r => this.input = r}

                    />
                 
                </View>
            </View>
        )
    }
}
export default MainInput