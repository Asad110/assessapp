import { StyleSheet } from "react-native";
import vh from "../../Units/vh";
import vw from "../../Units/vw";
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 5.5 * vh,
        backgroundColor: '#FFF',
        borderRadius: 1.5 * vw,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "space-between",
        shadowColor: "#000",
        shadowOffset: {
            width: 1 * vw,
            height: 3,
        },
        shadowOpacity: 0.28,
        shadowRadius: 2.00,

        elevation: 2,
    },
    field: {
        width: '85%',

        marginHorizontal: vw * 3,
        fontSize: 1.5 * vh,
        // top:0.3*vh,
        textAlignVertical: 'center',
        padding: 0,
        margin: 0,
        // paddingHorizontal: 2 * vw,
        color: "#333333"

    },
    leftIcon: {
        marginLeft: 3 * vw,
        height: 1.5 * vh,
        resizeMode: "contain",
    },
    rightIcon: {
        marginRight: 3 * vw,
        height: 1.7 * vh,
        width: 1.7 * vh,

        resizeMode: "contain"
    },
    label: {
        fontSize: 1.4 * vh,
        color: "#424953"
    },
    redLabel: {
        color: "#ED1C24"
    },
    labelContainer: {
        // width: '50%',
        // paddingLeft: 3 * vw,
        // marginBottom: vh * .8,
    }
})
export default styles