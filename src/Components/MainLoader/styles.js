import {
    StyleSheet
} from 'react-native'
import vw from '../../Units/vw';


export default styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    viewBg: {
        backgroundColor: 'rgba(0,0,0,0.65)',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loaderContainer: {
        alignSelf: 'center',
        backgroundColor: 'white',
        height: '20%',
        width: '70%',
        elevation: 100,
        borderRadius: 10,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
        alignContent: 'center',

    },
    indicatorContainer: {
        justifyContent: 'center'
    },
    loadingContainerText: {
        alignContent: 'center',
        justifyContent: 'center',
        marginRight: 10 * vw
    },


})