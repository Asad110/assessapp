import React, { Component } from 'react'
import {
    View,
    ActivityIndicator,
    Modal,
} from 'react-native'
// import Modal from 'react-native-modal'
import styles from './styles'
import { connect } from 'react-redux'
import { LOADING } from '../../Redux/Actions/types'
class MainLoader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false
        }
        this.timer = null
    }
    show = () => {
        this.setState(prev => {
            return {
                ...prev,
                visible: true
            }
        })
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps != this.props) {
            if (nextProps.loading == true) {
                this.showHandler()
            } else {
                // clear timer if still running
                this.hideHandler()
            }
            return true;
        } else {
            return false
        }
    }
    showHandler = () => {
        // //StatusBar.setHidden(true)
        this.timer = setTimeout(() => {
            // console.log('self destruction')
            // if(this.props.auto)
            // this.props.toggleLoader(false)        
            this.timer = null;
        }, 8000)
    }

    hideHandler = () => {
        if (this.timer != null) {
            clearTimeout(this.timer)
        }
    }
    hide = () => {
        this.setState(prev => {
            return {
                ...prev,
                visible: false
            }
        })
    }

    render() {
        // console.log(this.props)

        return (
            <Modal key={"Loader"} transparent={true} visible={this.props.loading} style={styles.container} >
                {/* {this.renderStatusBar()} */}
                <View style={styles.viewBg}>
                    <ActivityIndicator size='large' color={"#ED1C24"} />
                </View>
            </Modal>
        )

    }
}
mapDispatchtoProp = (disptach) => {
    return {
        toggleLoader: (value) => { disptach({ type: LOADING, value: value }) }
    }
}
mapStateToProps = (state) => {
    const { User, General } = state

    // console.log("General state", state);

    return {

        user_data: User.user,
        loading: General.loading
    }
}
export default connect(mapStateToProps, mapDispatchtoProp)(MainLoader)