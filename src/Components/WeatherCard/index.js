import React from 'react'
import { View, Image, Text } from 'react-native';
import styles from './styles'
import Utils from '../../Utils';




export default ({ item, index }) => {




    let { weather_state_abbr, the_temp, weather_state_name } = item;

    let imageUrl = Utils.asset_url + weather_state_abbr + '.png'

    console.log("ITEM:", item);


    const _renderWeatherImage = () => (
        <Image source={weather_state_abbr ? { uri: imageUrl } : ''} style={styles.mainImageStyles} />
    )


    return (<View style={styles.container}>
        <View>

            {_renderWeatherImage()}

            <Text style={styles.mainText}>{weather_state_name}</Text>
        </View>
        <Text style={styles.tempText} >{parseFloat(the_temp).toFixed(2)}</Text>

    </View>)
}