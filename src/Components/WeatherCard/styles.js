import { StyleSheet } from "react-native";
import vh from "../../Units/vh";
import vw from "../../Units/vw";
export default style = StyleSheet.create({
    container: {
        height: 25 * vh,
        margin: 2 * vh,
        padding: 1 * vh,
        borderRadius: 2 * vh,
        backgroundColor: "#FFFF",
        // justifyContent: 'space-between',
        width: 70 * vw,
        alignItems: 'center',
        flexDirection: 'row',
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,

        elevation: 6,
    },
    mainText: {
        fontSize: 3 * vh,
        color: 'lightblue',
        textAlign: 'left'

    },
    tempText: {
        marginHorizontal: 10 * vw,
        fontSize: 2.5 * vh,
        color: '#404040',
        textAlign: 'left'
    },
    mainImageStyles: {
        resizeMode: 'contain',
        width: 20 * vw,
        height: 20 * vh
    }

})