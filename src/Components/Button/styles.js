import { StyleSheet } from "react-native";
import vh from "../../Units/vh";
import vw from "../../Units/vw";

export default style = StyleSheet.create({
    container: {
        width: "100%",
        backgroundColor: "#ED1C24",
        height: vh * 6.5,
        borderRadius: vw * 1,
        paddingHorizontal:1*vw,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,

        elevation: 6,
    },
    label: { 
        fontSize: vw * 3.5,
         color: "white" }
})