import React from 'react'
import {
    Text,
    View
} from 'react-native'
import styles from './styles'
import TouchableHOC from '../../Components/TouchableHOC'



export default (props) => {
    return (
        <TouchableHOC style={[styles.container, props.styles]} onPress={props.onPress} disabled={props.disabled} {...props}>
            <Text style={[styles.label, props.labelStyle]}>{props.title}</Text>
        </TouchableHOC>
    )
}