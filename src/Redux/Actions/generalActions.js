import api from '../../Api/api';
import {
  GET_WEATHER,
  LOADING
} from './types';
import { persistConfig, store } from '../store';
import { getStoredState } from 'redux-persist';
import Utils from '../../Utils';

export const generalActions = {

  // ************* Get Weather ***************

  getWeather: () => {
    return (dispatch) => {
      dispatch({ type: LOADING, value: true })
      api.get(
        `location/1940345`,
        (success) => {
          console.log('Success get Weather', success);
          if (success.data) {
            dispatch({
              type: GET_WEATHER,
              payload: success.data,
            });
          } else {
            dispatch({
              type: GET_WEATHER,
              payload: success,
            });
          }
          dispatch({ type: LOADING, value: false })
          return;
        },
        (error) => {
          console.log('Get Weather error', error);
          dispatch({ type: LOADING, value: false })
          return;
        },
      );
    };
  },

};
