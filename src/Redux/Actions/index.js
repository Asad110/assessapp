const { userActions } = require("./userActions");
const { generalActions } = require("./generalActions");

module.exports = {
    employeeActions: userActions,
    weatherActions: generalActions,
}