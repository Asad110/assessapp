import api from '../../Api/api';
import {

  GET_USERS,
  CREATE_USER,
  UPDATE_USER,
  REMOVE_USER
} from './types';


export const userActions = {

  // ************* Get Employees ***************
  getUsers: () => {
    return (dispatch) => {


      dispatch({
        type: GET_USERS,
      });

    };
  },

  // ************* Create a new EmployeeD ***************
  createNewUser: (data) => {
    return (dispatch) => {

      dispatch({
        type: CREATE_USER,
        payload: data
      });

    };
  },

  // ************* Remove an EmployeeD ***************
  deleteUser: (id) => {
    console.log("action remove", id);
    return (dispatch) => {


      dispatch({
        type: REMOVE_USER,
        payload: id

      });

    };
  },

  // ************* Update an EmployeeD ***************
  updateUser: (data, pass) => {
    return (dispatch) => {


      dispatch({
        type: UPDATE_USER,
        payload: data
      });

      pass(true)
    };
  },


};
