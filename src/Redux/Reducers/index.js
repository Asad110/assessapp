import { combineReducers } from 'redux'
import { General } from './General'
import { User } from './User'

export default combineReducers({
  General,
  User
})


// export const migrations = {
//   1: (state) => {
//     // console.log("Migration running:", state);
//     return {
//       ...state,
//       General: {
//         ...state.General,
//         cart_state: {
//           cartArray: [],
//           total_weight: 0,
//           total_amount: 0
//         }
//       }

//     }
//   }
// }