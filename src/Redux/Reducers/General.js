import { GET_WEATHER, LOADING } from "../Actions/types";


let initialState = {

  loading: false,
  weather_data: null

}




export const General = (state = initialState, action) => {



  switch (action.type) {



    case LOADING: {
      console.log("LOADING action", action.value);
      return {
        ...state,
        loading: action.value,
      }
    }


    case GET_WEATHER: {
      console.log("GET_WEATHER action", action);
      state.weather_data = action.payload
      return {
        ...state,
        weather_data: { ...state.weather_data }
      }

    }

    default:
      return state
  }

}