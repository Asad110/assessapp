import { GET_USERS, CREATE_USER, REMOVE_USER, UPDATE_USER } from "../Actions/types"

let initialState = {

  users: require('../Services.json').employees,
  access_token: null,

}

export const User = (state = initialState, action) => {

  switch (action.type) {

    case GET_USERS: {
      console.log("GET_USERS action", action);
      return state
    }

    case CREATE_USER: {
      console.log("CREATE_USER action", action);
      let new_user = action.payload
      return {
        ...state,
        users: [...state.users, new_user]
      }
    }

    case REMOVE_USER: {
      console.log("REMOVE_USER action", action);

      return {
        ...state,
        users: state.users.filter((each, id) => id !== action.payload),
      }
    }


    case UPDATE_USER: {
      let index = state.users.findIndex((user, index) => String(action.payload.id) == String(index))
      state.users[Number(index)] = action.payload
      return { ...state, users: [...state.users] }
    }

    default:
      return state
  }

}