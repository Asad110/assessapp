import AsyncStorage from '@react-native-community/async-storage';

const appendParametersToUrl = (params, url) => {
    let parameter = ""
    let index = 0

    Object.entries(params).forEach(
        ([key, value]) => {
            let val = index === 0 ? "?" + key + "=" + value : "&" + key + "=" + value
            let prev = parameter
            parameter = prev + val
            index++
        }
    )

    return url + parameter

}

const base_url = 'https://www.metaweather.com/api/';


const asset_url = 'https://www.metaweather.com/static/img/weather/png/';

const persistConfig = {
    key: "rafflegobuyer-storage-root",
    storage: AsyncStorage,
    timeout: 10000,

}

const regex = {
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    alphabets: /^[a-zA-Z ]*$/,
    phone: /^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm,
    numbers: /^[0-9]+$/
}


const setFormData = (data) => {
    let formdata = new FormData()
    for (const [key, value] of Object.entries(data)) {
        // console.log(`${key}: ${value}`);
        formdata.append(key, value)
    }
    console.log("frmdata", formdata);
    return formdata
}





const Utils = {
    regex: regex,
    persistConfig,
    base_url,
    setFormData,
    appendParametersToUrl,
    asset_url

}
export default Utils;