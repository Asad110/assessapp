import React, { useEffect } from 'react'
import { View, Text, FlatList, RefreshControl } from 'react-native'
import Button from '../../Components/Button'
import styles from './styles'
import { useDispatch, useStore } from 'react-redux'
import { generalActions } from '../../Redux/Actions/generalActions'
import vw from '../../Units/vw'
import vh from '../../Units/vh'
import WeatherCard from '../../Components/WeatherCard'

export default (props) => {


    // const [loading,]

    const dispatch = useDispatch();

    const store = useStore().getState();


    const _refreshWeatherData = () => dispatch(generalActions.getWeather());

    useEffect(() => {
        _refreshWeatherData()
    }, [])


    let weather_data = store.General?.weather_data || _refreshWeatherData();

    const renderWeatherSummary = () => <FlatList keyExtractor={(item, index) => String(index)} refreshControl={<RefreshControl
        refreshing={false}

        onRefresh={_refreshWeatherData}
    />} showsVerticalScrollIndicator={false} data={weather_data?.consolidated_weather} renderItem={({ item, index }) => <WeatherCard item={item} index={index} />} />

    console.log("Weather Screen: ", weather_data)


    return (

        < View style={styles.container} >
            {weather_data && renderWeatherSummary()}
            < Button styles={{ width: 30 * vw, height: 5 * vh }
            } onPress={_refreshWeatherData} title="Refresh" />
            {/* <Text style={styles.mainText}>Current Weather</Text> */}
        </View >
    )
}