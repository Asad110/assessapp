import { StyleSheet } from "react-native";
import vh from "../../Units/vh";
import vw from "../../Units/vw";
export default style = StyleSheet.create({
    container: {
        flex: 1,
        padding:2*vh,
        backgroundColor: "transparent",
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainText: {
        fontSize: 2.5 * vh,
        color: '#FF0FFF'
    },
    mainImageStyles: {
        resizeMode: 'contain',
        width: 100 * vw,
        height: 30 * vh
    }

})