import { StyleSheet } from "react-native";
import vh from "../../Units/vh";
import vw from "../../Units/vw";
export default style = StyleSheet.create({
    container: {
        flex: 1,
        // width: 70 * vw,
        backgroundColor: "#FFFFFF",
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainText: {
        fontSize: 2.2 * vh
    }

})