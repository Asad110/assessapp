import React, { useEffect } from 'react'
import { View, Text, FlatList, RefreshControl } from 'react-native'
import Button from '../../Components/Button'
import vh from '../../Units/vh'
import vw from '../../Units/vw'
import styles from './styles'

export default ({ navigation }) => {



    return (
        <View style={styles.container}>


            <Button onPress={() => navigation.navigate("EmployeeScreen")} styles={{ backgroundColor: '#6B38FB', width: 40 * vw, margin: 5 * vh }} title="Employees" />
            <Button onPress={() => navigation.navigate("WeatherScreen")} styles={{ backgroundColor: '#6B38FB', width: 40 * vw, margin: 5 * vh }} title="Weather" />


        </View>
    )
}