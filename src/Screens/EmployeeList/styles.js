import { StyleSheet } from "react-native";
import vh from "../../Units/vh";
import vw from "../../Units/vw";
export default style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#4A6572",
        justifyContent: 'center',
        alignItems: 'center'
    },
    mainText: {
        fontSize: 2.2 * vh
    }

})