import React, { useEffect, useReducer, useState } from 'react'
import { View, Text, FlatList, RefreshControl } from 'react-native'
import Button from '../../Components/Button'
import styles from './styles'
import { connect } from 'react-redux'
import { generalActions } from '../../Redux/Actions/generalActions'
import EmployeeCard from '../../Components/EmployeeCard'
import { userActions } from '../../Redux/Actions/userActions'
import vh from '../../Units/vh'
import AddPopup from '../../Popups/AddPopup'

class EmployeeList extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            filter_above_25: false
        }
    }



    _addNewEmployee = (data, pass, fail) => {
        let new_user = {
            id: this.props.employee_data.length,
            name: data?.name,
            age: data?.age
        }
        this.props.createNewUser(new_user);
        pass("Employee Added Successfully");
    }

    _deleteEmployee = (index) => {
        // alert("Delete" + index)
        this.props.deleteEmployee(index);

    }

    _updateEmployee = (data, pass) => {
        // alert("Update" + index)
        this.props.updateUser(data, success => {
            pass("Employee Updated Successfully");

        });

    }


    _fetchEmployees = () => {
        this.props.fetchEmployees();

    }


    _getEmployeessData = () => {
        if (this.state.filter_above_25) {

            return this.props.employee_data.filter((each_emp) => parseInt(each_emp.age) > 25)
        }
        return this.props.employee_data
    }



    _renderUsers = () => <FlatList
        style={{ marginTop: 10 * vh }}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={() => <View style={{ backgroundColor: 'transparent', height: 5 * vh }}></View>}
        data={this._getEmployeessData()}
        renderItem={({ item, index }) => <EmployeeCard
            onDeletePressed={this._deleteEmployee}
            onUpdatePressed={() => this.updateEmpPopup.show(item)}
            item={item} index={index} />
        }
        keyExtractor={(item, index) => String(index)} refreshControl={< RefreshControl
            refreshing={false}
            onRefresh={this._fetchEmployees}

        />}
    />


    render() {
        console.log("User reducer: ", this.props.employee_data);
        return (
            <View style={styles.container} >

                <AddPopup onSuccess={this._addNewEmployee} ref={r => this.addEmpPopup = r} />

                <AddPopup update={true} onSuccess={this._updateEmployee} title="Update Employee" ref={r => this.updateEmpPopup = r} />


                { this._renderUsers()}

                < View >
                    <Button onPress={() => this.addEmpPopup.show()} styles={{ backgroundColor: '#6B38FB', }} title="Add an Employee" />
                    <Button onPress={() => this.setState({ filter_above_25: !this.state.filter_above_25 })} styles={this.state.filter_above_25 ? { backgroundColor: '#6B38FB' } : { backgroundColor: 'lightgray', }} title="Toggle Above 25 Filter" />

                </View>
            </View >
        )
    }
}

const mapStateToProps = (state) => {


    const { User } = state

    return {

        employee_data: User?.users
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchEmployees: () => dispatch(userActions.getUsers()),
        deleteEmployee: (index) => dispatch(userActions.deleteUser(index)),
        updateUser: (data, success) => dispatch(userActions.updateUser(data, success)),
        createNewUser: (data) => dispatch(userActions.createNewUser(data))

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeList);