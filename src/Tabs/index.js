import React from 'react';
import {
    Notifications, 
    Home,
    Drawer,
    Profile, 
    RaffleDetail,
    NotificationDetail,
    EDrawing,
    LiveDrawing,
    LogDetail,
    RaffleLog,
    ViewAllRaffle
} from '../screens'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import vw from '../Units/vw';
import vh from '../Units/vh';
import { Fonts } from '../assets/fonts';
import { View, Image } from 'react-native';
import { icons, assets, tabIcons } from '../assets/images';
import { createStackNavigator } from '@react-navigation/stack';
import { options } from '../Navigation/NavigationHeader/index';
import TextMedium from '../Components/TextMedium';
import ContactUs from '../screens/ContactUs';
import Payment from '../screens/Payment';
import LogList from '../screens/LogList';
import OnGoingRaffle from '../screens/OngoingRaffle';
import RaffleHistory from '../screens/RaffleHistory';
import RaffleCancelled from '../screens/RaffleCancelled';
import TextRegular from '../Components/TextRegular';
import styles from './styles'
const HomeTabs = createStackNavigator();
const NotificationTabs = createStackNavigator();
const AccountTabs = createStackNavigator();

const Tab = createMaterialTopTabNavigator();
const HomeStack = () => {
    return (
        <HomeTabs.Navigator screenOptions={options}>
            <HomeTabs.Screen name="Home" component={Home} />

            <HomeTabs.Screen name="RaffleDetail" component={RaffleDetail} />

            <HomeTabs.Screen name="Payment" component={Payment} />
            
            <HomeTabs.Screen name="ViewAllRaffle" component={ViewAllRaffle} />


        </HomeTabs.Navigator>
    );
};
const NotificationStack = () => {
    return (
        <NotificationTabs.Navigator screenOptions={options}>
            <NotificationTabs.Screen name="Notifications" component={Notifications} />
            <NotificationTabs.Screen name="NotificationDetail" component={NotificationDetail} />
            <NotificationTabs.Screen name="EDrawing" component={EDrawing} />
            <NotificationTabs.Screen name="LiveDrawing" component={LiveDrawing} />
            

        </NotificationTabs.Navigator>
    );
};
const AccountStack = () => {
    return (
        <AccountTabs.Navigator screenOptions={options}>
            <AccountTabs.Screen name="Drawer" component={Drawer} />
            <AccountTabs.Screen name="ContactUs" component={ContactUs} />

            <AccountTabs.Screen name="Profile" component={Profile} />
            <AccountTabs.Screen name="RaffleLog" component={RaffleLog} />
            <AccountTabs.Screen name="LogList" component={LogList} />
            <AccountTabs.Screen name="OnGoingRaffle" component={OnGoingRaffle} />
            <AccountTabs.Screen name="RaffleHistory" component={RaffleHistory} />
            <AccountTabs.Screen name="RaffleCancelled" component={RaffleCancelled} />

            <AccountTabs.Screen name="LogDetail" component={LogDetail} />

        </AccountTabs.Navigator>
    );
};
export default Tabs = () => {
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarLabel: ({ focused, color, size }) => {
                    return (
                        <TextMedium style={[{  fontSize: vh * 1.3,
                            color: focused?'#ED1C24':'#999',}]}>{route.name}
                            </TextMedium>
                    );
                },
                tabBarIcon: ({ focused, color, size }) => {
                    console.log("route name", focused);
                    let iconName;
                    if (route.name == 'Store') {

                        iconName = focused ? tabIcons.homeActive : tabIcons.homeInactive;
                    }
                    else if (route.name === 'Notification') {
                        iconName = focused ? tabIcons.bellActive : tabIcons.bellInactive;

                    }
                    else if (route.name === 'Accounts') {
                        iconName = focused ? tabIcons.accountActive : tabIcons.accountInactive;

                    }
                    return (
                        <View style={{alignItems:"center",justifyContent:"flex-end",height:4*vh,paddingTop:0.4*vh}}>
                        <Image
                            source={iconName}
                            style={{
                                width: 2.2 * vh,
                                height: 2.2 * vh,
                                marginBottom: 0.4 * vh,
                                
                            }}
                            resizeMode="contain"
                        />
                          {route.name === 'Notification'?
                    <View style={styles.badgeView}>
                        <TextRegular style={[styles.badge]}>2</TextRegular>
                    </View> : null}
                        </View>
                    );
                },
            })}
            tabBarPosition="bottom"
            tabBarOptions={{
                allowFontScaling: false,
                indicatorStyle: {
                   backgroundColor:"#FFF"
                },

                activeTintColor: '#ED1C24',
                inactiveTintColor: '#000',

                iconStyle: {
                    width: 20 * vw,
                    height:3*vh,
                    padding: 0,
                    alignItems: 'center',
                    justifyContent: 'center',
                },
                showIcon: true,
                pressOpacity: 0.0,
                style: {
                    height: 7 * vh,
                    width: 100 * vw,
                    justifyContent:"center",
                    padding: 0,
                    elevation: 5,
                    backgroundColor: "#FFF"
                },

                labelStyle: {
                    fontFamily: Fonts.PM,
                    fontSize: vh * 1.3,
                    color: '#666',
                },
                tabStyle: {
                    margin: 0,
                    padding: 0,
                },
            }}
        >
            <Tab.Screen
                name="Store"
                component={HomeStack}
            />
            <Tab.Screen
                name="Notification"
                component={NotificationStack}
            />
            <Tab.Screen
                name="Accounts"
                component={AccountStack}
            />
        </Tab.Navigator>
    );
};
