import vw from '../Units/vw'
import vh from '../Units/vh'
import { Platform, StyleSheet } from 'react-native'
export default StyleSheet.create({
    badgeView:{
        elevation: 2,
        shadowColor: "#333",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.08,
        shadowRadius: 3,
        height: 2.1 * vh,
        width: 2.1* vh,
        borderColor:"#FFF",
        borderWidth:1.9,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:"#ED1C24",
        position: 'absolute',
        bottom: 1.5* vh,
        left:2*vw,
        borderRadius: 2.7/2 * vh,
      
     
    },
    badge: {
      
        color: '#fff',
        fontSize: 1.3 * vh,
        ...Platform.select({
            android:{
                marginTop:0.5*vh,
            },
            ios:{
                // marginTop:0.1*vh
            }
        })
      
    }

})